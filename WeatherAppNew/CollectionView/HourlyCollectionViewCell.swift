
import UIKit

class HourlyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    var dateHourly: String = ""
    var icon: String = ""
    
    func dateFormatter(date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH/d.MM"
        self.dateHourly = dateFormatter.string(from: date)
    }
    
    func configure(with object: HourlyWeatherModel) {
        guard let icon = object.weather?.first?.icon else {return}
        NetworkManager.shared.downloadImage(url: icon, image: weatherImage)
        guard let temp = object.temp else {return}
        guard let date = object.dt else {return}
        let currentDate = Date(milliseconds: date * 1000)
        dateFormatter(date: currentDate)
        
        
        self.timeLabel.text = "\(dateHourly)"
        self.temperatureLabel.text = "\(Int(temp))º"
        
    }
    
    
}
