
import UIKit
import CoreLocation

class MainViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var temperatureLbael: UILabel!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    private let locationManager = CLLocationManager()
    var offerModel: MainWeatherModel?
    var hourlyArray: [HourlyWeatherModel] = []
    var dailyArray: [DailyWeatherModel] = []
    var latitude: Double?
    var longitude: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.isHidden = true
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    
    @IBAction func reloadWeatherData(_ sender: UIButton) {
        showSpinnerView()
        setup()
        print("Work it")
    }
    
    func showSpinnerView() {
        spinner.isHidden = false
        spinner.color = .white
        spinner.startAnimating()
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        let location: CLLocationCoordinate2D = manager.location!.coordinate
        self.latitude = location.latitude
        self.longitude = location.longitude
        setup()
        
        
        print("locationssss = \(location.latitude) \(location.longitude)")
    }
    func setup() {
        weatherImage.addParalaxEffect()
        NetworkManager.shared.getWeather(latitude: self.latitude ?? 30, longitude: self.longitude ?? 100, completion:  {  (model) in
            self.offerModel = model
            guard let city = model?.timezone,
                  let description = model?.current?.weather?.first?.description,
                  let main = model?.current?.weather?.first?.main,
                  let temperature = model?.current?.temp,
                  let maxTemp = model?.daily?.first?.temp?.max,
                  let minTemp = model?.daily?.first?.temp?.min,
                  let hourlyArray = model?.hourly,
                  let dailyArray = model?.daily else {return}
            self.choiseImage(with: main)
            self.dailyArray = dailyArray
            self.hourlyArray = hourlyArray
            self.cityLabel.text = "\(city)"
            self.descriptionLabel.text = "\(description)".firstUppercased
            self.temperatureLbael.text = "\(Int(temperature))º"
            self.feelsLikeLabel.text = "Макс. \(Int(maxTemp))º, Мин. \(Int(minTemp))º"
            self.collectionView.reloadData()
            self.tableView.reloadData()
            self.spinner.stopAnimating()
            self.spinner.isHidden = true
        })
        
    }
    
    func choiseImage(with weather: String) {
        if weather == "Clouds" {
            weatherImage.image = UIImage(named: "cloudy")
        } else if weather == "Clear" {
            weatherImage.image = UIImage(named: "sunny")
        } else if weather == "Rain" {
            weatherImage.image = UIImage(named: "rain")
        } else if weather == "Snow" {
            weatherImage.image = UIImage(named: "snow")
        } else if weather == "Drizzle" {
            weatherImage.image = UIImage(named: "rain")
        } else if weather == "Thunderstorm" {
            weatherImage.image = UIImage(named: "thunderstorm")
        }
    }
    
    
}
extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.hourlyArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourlyCollectionViewCell", for: indexPath) as? HourlyCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.configure(with: hourlyArray[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.width / 8)
        let height = self.collectionView.frame.height
        return CGSize(width: width, height: height)
    }
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dailyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DailyTableViewCell", for: indexPath) as? DailyTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(with: dailyArray[indexPath.row])
        return cell
    }
    
    
}
