import Foundation
import Kingfisher

class NetworkManager {
    var weather: MainWeatherModel?
    private init() {}
    static let shared: NetworkManager = NetworkManager()
    
    func getWeather(latitude: Double, longitude: Double, completion: @escaping (MainWeatherModel?) -> ()) {
//        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/onecall?lat=\(latitude)&lon=\(longitude)&units=metric&exclude=minutely&appid=80fa3e23804079f27e8ccd16eb117295") else {
//            return
//        }
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/onecall?lat=\(latitude)&lon=\(longitude)&units=metric&exclude=minutely&lang=ru&appid=80fa3e23804079f27e8ccd16eb117295") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            if error == nil, let data = data {
                do {
            let decoderOfferModel = try JSONDecoder().decode(MainWeatherModel.self, from: data)
                    self!.weather = decoderOfferModel
                    DispatchQueue.main.async {
                        completion(decoderOfferModel)
                    }
                    
                } catch {
                    print(error)
                }
        }
        }
        task.resume()
    }
    
    func downloadImage(url: String, image: UIImageView) {
        let url = URL(string: "https://openweathermap.org/img/wn/\(url)@2x.png")
        image.kf.setImage(with: url)
    }
}
