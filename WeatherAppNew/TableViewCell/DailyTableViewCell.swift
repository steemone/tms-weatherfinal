
import UIKit

class DailyTableViewCell: UITableViewCell {

    
    @IBOutlet weak var weekDayLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    
    var weekDay: String = ""
//    var otherweekdayStrings: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(with object: DailyWeatherModel) {
        guard let date = object.dt else {return}
        let currentDate = Date(milliseconds: date * 1000)
        dateFormatter(date: currentDate)
        guard let icon = object.weather?.first?.icon else {return}
        NetworkManager.shared.downloadImage(url: icon, image: weatherImage)
        guard let rain = object.pop else {return}
        self.rainLabel.text = "\(Int(rain * 100)) %"
        
        guard let dayTemperature = object.temp?.day else {return}
        guard let nightTemperature = object.temp?.night else {return}
        
        self.maxTempLabel.text = "\(Int(dayTemperature))º"
        self.minTempLabel.text = "\(Int(nightTemperature))º"
        self.weekDayLabel.text = "\(weekDay)"
        
    }
    
//    func formatterTest() {
//        let today = Date()
//        let formatter = DateFormatter()
//        formatter.dateFormat = "EEEE"
//        for i in 1...6 {
//            let timeIntervalToAdd = TimeInterval(i * 86400)
//            self.otherweekdayStrings.append(formatter.string(from: today.addingTimeInterval(timeIntervalToAdd)))
//        }
//    }
    func dateFormatter(date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM"
        self.weekDay = dateFormatter.string(from: date)
    }

}
