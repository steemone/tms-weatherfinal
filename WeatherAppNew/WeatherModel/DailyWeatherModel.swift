
import Foundation

class DailyWeatherModel: Codable {
    var dt: Int?
    var sunrise: Int?
    var sunset: Int?
    var moonrise: Int?
    var moonset: Int?
    var moonphase: Double?
    var temp: TemperatureModel?
    var feelsLike: FeelsLikeModel?
    var pressure: Int?
    var humidity: Int?
    var dew_point: Double?
    var uvi: Double?
    var clouds: Int?
    var visibility: Int?
    var wind_speed: Double?
    var wind_deg: Int?
    var wind_gust: Double?
    var weather: [WeatherModel]?
    var pop: Double?
    var rain: Double?
}
