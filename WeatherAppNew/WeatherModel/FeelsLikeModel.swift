
import Foundation


class FeelsLikeModel: Codable {
    var day: Double?
    var night: Double?
    var eve: Double?
    var morn: Double?
}
